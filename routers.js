import React from 'react'

import {BrowserRouter, Route , Redirect , Switch} from 'react-router-dom';

import { auth } from './../services/auth';

// Components
import Login from './../pages/Login'

function isAuthenticated(){
    console.log(auth);
    return auth
}
/**
 * Private Route é um component que irá retornar a Route para determinado local da aplicação, ele recebe
 * um valor boleano. Limitamos ele para quando receber true ele  será redirecionado para a rota que deseja
 * no nosso caso autenticado.
 * 
 *  ({ component: Component, ...rest }) -> Irá transformar component em Component com C maiúsculo para ficar no padrão
 *  Dentro do component Route na propriedade render={} será atribuido uma arrow function para validar nossa função 
 *  isAuthenticated(props => isAuthenticated() ? (<Component {...props} />))
 * 
 *  se ela retornar True , será redirecionado para o component com todas as propriedades
 * 
 *  se não, ele sofrerá um redirect para raiz do projeto e levar com ele o 
 *  
 *  Redefinimos o método render recebemos as props.
 * 
 * Com redirect pathname raiz e passamos tbm state ele garante o histórico de rotas dele
 * 
 * from: props.location isso nos garante que o usuário não perca seu histórico de navegação.
 * 
 */

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

export default function Router() {
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login} />
                <PrivateRoute path="/login" component={() => <h1>Autenticado</h1>} />
            </Switch>
        </BrowserRouter>
    )
}
